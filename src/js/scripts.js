const images = ["./img/bo.jpg", "./img/img1.jpg", "./img/img2.jpg", "./img/img3.jpg"]

window.onload = function () {
    // document.body.innerHTML = document.body.innerHTML + "<div> RABBIT </div>";
    window.setInterval(() => {
        switchImg(brownRabbit, randomImg(rabbits))
    }, 1000);

    switchImg(randomImage, randomImg(images));


};

$(document).ready(function () {
    $(".button").click(function () {
        $(this).click(clickButton(this));
    })
})


function switchImg(id, image) {
    // console.log("switch");
    $(id).attr("src", image);
}

function randomImg(images) {
    return images[Math.floor(Math.random() * images.length)]
}

function clickButton(button) {
    toggleArticle(button);
}

function toggleArticle(button) {
    console.log("toggle");
    if (button.innerHTML == "Show article") {
        button.innerHTML = "Hide article";
        $(button).parent().children("p").css('display', 'block');
    } else {
        button.innerHTML = "Show article";
        $(button).parent().children("p").css('display', 'none');
    }

}